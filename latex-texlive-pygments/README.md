# latex-texlive-pygments
Dockerfile for a Docker image providing a full TeX Live installation with Pygments (and therefore minted package) support.
TeX Live will be downloaded via CTAN mirrors, leading to an up-to-date LaTeX major release installation by the time of image building.
Build may fail at times because of CTAN choosing a broken mirror.

The image can be found on Docker Hub: [phist91/latex-texlive-pygments](https://hub.docker.com/r/phist91/latex-texlive-pygments)

The image tags specify the day on which the image was built, e.g., `2024-02-01` denotes February 1, 2024.
This should be helpful when older package versions are needed.
Since February 2024, you can also specify the architecture (currently, `amd64` or `arm64`), but if you do not, it will be picked automatically.


### Building the images locally
If you want to build the image locally, just use the `Makefile` in the folder that fits your computer's architecture.
It will be tagged as described above.
Alternatively, specify your own `docker build` command.


## Usage

### For compiling local LaTeX documents
You can create a Docker container using the image and pass a directory from your machine as a volume to execute LaTeX binaries in that directory.
For example, if you want to run the command `latexmk` in the folder `/path/to/local/texfiles`:

```shell
docker run --rm --volume=/path/to/local/texfiles:/texworkdir -it phist91/latex-texlive-pygments:latest /bin/bash -c "cd /texworkdir && latexmk"
```

See the [ltxmk.sh example script](ltxmk.sh) for an example how to use this command in a script.

### For GitLab CI
You can use the image for your Docker-powered GitLab CI pipeline.
For example, your `.gitlab-ci.yml` could look like this:

```yaml
latex:
  image: phist91/latex-texlive-pygments:latest
  script:
    - latexmk
  artifacts:
    paths:
      - my-document.pdf
    expire_in: 30 days
```