#!/bin/bash

DOCKERIMAGE="phist91/latex-texlive-pygments:latest"

# Spawns container and executes command.
run-container() {
  docker run --rm --volume="$(pwd)":/texworkdir -it $DOCKERIMAGE /bin/bash -c "cd /texworkdir && $1"
}

# Prints help on console.
print-help() {
  echo ""
  echo "Usage:   ltxmk.sh COMMAND"
  echo ""
  echo "Executes a command from a $DOCKERIMAGE container in the current directory."
  echo ""
  echo "Options for COMMAND:"
  echo "  b, build,       Executes latexmk."
  echo "  c, clean        Executes latexmk -c."
  echo "  C, full-clean   Executes latexmk -C."
  echo ""
  echo "Any other value for COMMAND will show this information."
  echo ""
}

#######################

if [[ "$#" -ne 1 ]]; then
  print-help
else
  case "$1" in
    "build" | "b")
      run-container "latexmk" ;;
    "clean" | "c")
      run-container "latexmk -c" ;;
    "full-clean" | "C")
      run-container "latexmk -C" ;;
    *)
      print-help
  esac
fi
