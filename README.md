# Dockerfiles
Repository for Dockerfiles, especially for Docker images published via [Docker Hub](https://hub.docker.com/u/phist91).

## List of Dockerfiles
- [jekyll](https://hub.docker.com/r/phist91/jekyll): Jekyll, for both amd64 and arm64 platform.
- [latex-texlive-pygments](https://hub.docker.com/r/phist91/latex-texlive-pygments): TeX Live with Pygments.