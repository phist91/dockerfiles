# jekyll
Dockerfile for a Docker image providing Jekyll, based on Ubuntu.

The image can be found on Docker Hub: [phist91/jekyll](https://hub.docker.com/r/phist91/jekyll)

The image tags specify the day on which the image was built, e.g., `2024-02-01` denotes February 1, 2024.
This should be helpful when older versions are needed.
You can also specify the architecture (currently, `amd64` or `arm64`), but if you do not, it will be picked automatically.


### Building the images locally
If you want to build the image locally, just use the target `image-amd64` or `image-arm64` in the `Makefile`, depending on your computer's architecture.
It will be tagged as described above.
Alternatively, specify your own `docker build` command.


## Usage

### For local developing
To use the image for local development, mount the root folder of your Jekyll project to the containers' `/jworkdir` folder and specify a Docker volume for Ruby Gems used by Jekyll.
For example, to download the required Gems first, you can use this command: 

```shell
docker run -it --rm --volume="/path/to/project:/jworkdir" --volume="jekyll-gems:/gems" phist91/jekyll:latest /bin/bash -c "cd /jworkdir && bundle install"
```

Afterwards, you can build your project:
```shell
docker run -it --rm --volume="/path/to/project:/jworkdir" --volume="jekyll-gems:/gems" phist91/jekyll:latest /bin/bash -c "cd /jworkdir && bundle exec jekyll serve"
```

See the [example Makefile](Example-Makefile) for a Makefile template you can copy into the root folder of your Jekyll project.


### For GitLab CI
You can use the image for your Docker-powered GitLab CI pipeline.
For example, your `.gitlab-ci.yml` could look like this:

```yaml
jekyll:
  image: phist91/jekyll:latest
  script:
    - bundle install
    - bundle exec jekyll build
  artifacts:
    paths:
      - _site
    expire_in: 30 days
```